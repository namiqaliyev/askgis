﻿using AskGis.DAL;
using AskGis.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AskGis.Business
{
    public abstract class BaseBusiness<TEntity> : IBaseBusiness<TEntity> where TEntity: EntityBase
    {
        public BaseBusiness()
        {
            Uow = new UnitOfWork();
            Repository = Uow.CreateRepository<TEntity>();
        }

        protected UnitOfWork Uow { get; set; }

        protected IRepository<TEntity> Repository { get; set; }

        public IEnumerable<TEntity> All()
        {
            return Repository.Select(p => p.IsAlive).ToArray();
        }

        public void Delete(int id)
        {
            var entity = Repository.Find(p=>p.Id == id);
            if(entity != null)
            {
                Repository.Delete(entity);
                Uow.SaveChanges();
            }
        }

        public TEntity GetById(int id)
        {
            return Repository.Find(p => p.Id == id);
        }
    }
}
