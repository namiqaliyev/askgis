﻿using AskGis.Config;
using AskGis.DAL;
using AskGis.Entities;
using AskGis.Entities.InputModels;
using AskGis.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AskGis.Business
{
    public class AuthBusiness: IAuthBusiness
    {
        private readonly IUnitOfWork uow;
        private readonly IRepository<User> userRepository;
        private readonly IRepository<LoginActivity> loginActivityRepository;

        public AuthBusiness()
        {
            uow = new UnitOfWork();
            userRepository = uow.CreateRepository<User>();
            loginActivityRepository = uow.CreateRepository<LoginActivity>();
        }

        public bool IsTokenFamiliar(string token)
        {
            var activity = loginActivityRepository.Find(p => p.Token == token && p.IsActive && p.IsAlive);
            if (activity == null)
                return false;
            var loginDuration = Configs.AppSettings.LoginDuration;
            return activity.LoginTime.AddHours(loginDuration) > DateTime.Now;
        }

        public User GetUserByUsername(string username)
        {
            var result = userRepository.Find(p => p.Username == username);
            return result;
        }

        public void Login(User user, string token)
        {
            loginActivityRepository.Insert(new LoginActivity
            {
                User = user,
                Token = token,
                LoginTime = DateTime.Now,
                IsActive = true
            });
            uow.SaveChanges();
        }

        public void Logout(string token)
        {
            var activity = loginActivityRepository.Find(p => p.Token == token);
            activity.IsActive = false;
            activity.ModifiedAt = DateTime.Now;
            loginActivityRepository.Update(activity);
            uow.SaveChanges();
        }

        public User GetUserByToken(string token)
        {
            var activity = loginActivityRepository.Find(p => p.Token == token && p.IsActive && p.IsAlive);
            return activity.User;
        }
    }
}
