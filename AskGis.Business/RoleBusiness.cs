﻿using AskGis.DAL;
using AskGis.Entities;
using AskGis.Entities.InputModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AskGis.Business
{
    public class RoleBusiness : BaseBusiness<Role>, IRoleBusiness
    {
        private readonly IRepository<Permission> permissionRepository;

        public RoleBusiness()
        {
            permissionRepository = Uow.CreateRepository<Permission>();
        }

        public void Save(RoleSaveInput input)
        {
            
            Role role = null;
            if(input.RoleId == 0)
            {
                role = new Role
                {
                    RoleName = input.RoleName
                };
                Repository.Insert(role);
            }
            else
            {
                role = Repository.Find(p => p.Id == input.RoleId);
                role.RoleName = input.RoleName;
                role.Permissions.RemoveAll(p=>input.Permissions.Contains(p.Id));
                
                Repository.Update(role);
            }
            var perms = permissionRepository.Select(p => input.Permissions.Contains(p.Id));
            role.Permissions.AddRange(perms);
            Uow.SaveChanges();
        }
    }
}
