﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AskGis.Entities;
using AskGis.Entities.InputModels;

namespace AskGis.Business
{
    public interface IRoleBusiness : IBaseBusiness<Role>
    {
        void Save(RoleSaveInput input);
    }
}
