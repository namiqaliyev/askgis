﻿using AskGis.DAL;
using AskGis.Entities;
using AskGis.Entities.InputModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AskGis.Business
{
    public class PersonBusiness : BaseBusiness<Person>, IPersonBusiness
    {
        public void Save(PersonSaveInput input)
        {
            Person person = input.PersonId == 0 ? new Person() : Repository.Find(input.PersonId);

            person.Id = input.PersonId;
            person.FirstName = input.Firstname;
            person.Fathername = input.Fathername;
            person.LastName = input.Lastname;
            person.IsStaff = true;
            person.IsGenderMale = input.Gender == 1;

            if (input.PersonId == 0)
            {
                Repository.Insert(person);
            }
            else
            {
                Repository.Update(person);
            }

            Uow.SaveChanges();

        }
    }
}
