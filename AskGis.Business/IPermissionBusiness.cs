﻿using AskGis.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AskGis.Business
{
    public interface IPermissionBusiness: IBaseBusiness<Permission>
    {
    }
}
