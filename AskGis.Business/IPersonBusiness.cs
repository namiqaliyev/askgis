﻿using AskGis.Entities;
using AskGis.Entities.InputModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AskGis.Business
{
    public interface IPersonBusiness: IBaseBusiness<Person>
    {
        void Save(PersonSaveInput input);
    }
}
