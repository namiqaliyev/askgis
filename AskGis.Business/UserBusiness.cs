﻿using AskGis.DAL;
using AskGis.Entities;
using AskGis.Entities.InputModels;
using AskGis.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AskGis.Business
{
    public class UserBusiness : BaseBusiness<User>, IUserBusiness
    {
        private readonly IRepository<Person> personRepository;
        private readonly IRepository<Role> roleRepository;

        public UserBusiness()
        {
            personRepository = Uow.CreateRepository<Person>();
            roleRepository = Uow.CreateRepository<Role>();
        }

        public void ChangePassword(ChangePasswordInput input)
        {
            var user = Repository.Find(input.UserId);
            user.Password = EncDec.BCrypt_Hash(input.Password);
            Repository.Update(user);
            Uow.SaveChanges();
        }

        public void Save(NewUserInput newUser)
        {
            if (newUser.UserId == 0)
            {
                var hasUser = Repository.Select(p => p.Username == newUser.Username).Count() > 0;
                if (hasUser)
                    throw new Exception("Bu istifadəçi sistemdə mövcuddur.");

                var user = new User
                {
                    Password = EncDec.BCrypt_Hash(newUser.Password),
                    Username = newUser.Username,
                    Person = personRepository.Find(p => p.Id == newUser.PersonId),
                    Role = roleRepository.Find(p => p.Id == newUser.RoleId)
                };
                Repository.Insert(user);
            }
            else
            {
                var user = Repository.Find(p => p.Id == newUser.UserId);
                user.Username = newUser.Username;
                user.Person = personRepository.Find(p => p.Id == newUser.PersonId);
                user.Role = roleRepository.Find(p => p.Id == newUser.RoleId);
                Repository.Update(user);
            }

            Uow.SaveChanges();
        }
    }
}
