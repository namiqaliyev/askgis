﻿using AskGis.Entities;
using AskGis.Entities.InputModels;

namespace AskGis.Business
{
    public interface IAuthBusiness
    {
        void Login(User user, string token);
        void Logout(string token);
        User GetUserByUsername(string username);
        User GetUserByToken(string token);
        bool IsTokenFamiliar(string token);
    }
}