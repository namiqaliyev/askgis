﻿using AskGis.Entities;
using AskGis.Entities.InputModels;
using System.Collections;
using System.Collections.Generic;

namespace AskGis.Business
{
    public interface IUserBusiness: IBaseBusiness<User>
    {
        void Save(NewUserInput newUser);
        void ChangePassword(ChangePasswordInput input);
    }
}