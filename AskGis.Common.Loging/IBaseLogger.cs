﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AskGis.Common.Loging
{
    public interface IBaseLogger
    {
        void Write(string text);
    }
}
