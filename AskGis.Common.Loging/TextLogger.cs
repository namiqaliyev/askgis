﻿using AskGis.Config;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AskGis.Common.Loging
{
    public class TextLogger : IBaseLogger
    {
        private readonly string logPath;

        public TextLogger()
        {
            logPath = Configs.AppSettings.LogPath;
            logPath = !IsValidPath(logPath) ? GetCurrentDirectory() : logPath;
        }

        public void Write(string text)
        {
            var fileName = GenerateLogFilename();
            var file = File.AppendText(fileName);
            var logText = CreateLogText(text);
            file.WriteLine(logText);
            file.Flush();
            file.Close();
        }

        private string GetCurrentDirectory()
        {
            var path = new Uri(GetType().Assembly.CodeBase).LocalPath;
            path = Path.GetDirectoryName(path);

            return path;
        }

        private string GenerateLogFilename()
        {
            var filename = DateTime.Now.ToString("yyyy-MM-dd") + ".log";
            filename = Path.Combine(logPath, filename);
            return filename;
        }

        private string CreateLogText(string text)
        {
            var logText = "[ ";
            logText += DateTime.Now.ToString();
            logText += " " + text + " ]";

            return logText;
        }

        private bool IsValidPath(string path)
        {
            if (string.IsNullOrEmpty(path))
                return false;

            if (!Directory.Exists(path))
                return false;

            return true;
        }
    }
}
