﻿using AskGis.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace AskGis.DAL
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity: EntityBase
    {
        private readonly DbContext dbContext;

        public Repository(DbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        public IQueryable<TEntity> All()
        {
            return Collection;
        }

        public void Delete(TEntity entity)
        {
            entity.ModifiedAt = DateTime.Now;
            entity.IsAlive = false;

            dbContext.Entry(entity).State = EntityState.Modified;
        }

        public TEntity Find(Expression<Func<TEntity, bool>> predicate)
        {
            return Collection.FirstOrDefault(predicate);
        }

        public void Insert(TEntity entity)
        {
            var now = DateTime.Now;

            entity.InsertAt = now;
            entity.ModifiedAt = now;
            entity.IsAlive = true;
            Collection.Add(entity);
        }

        public IQueryable<TEntity> Select(Expression<Func<TEntity, bool>> predicate)
        {
            return Collection.Where(predicate);
        }

        public void Update(TEntity entity)
        {
            entity.ModifiedAt = DateTime.Now;
            dbContext.Entry(entity).State = EntityState.Modified;
        }

        public object RunQuery<T>(string procName, object paramList)
        {
            var result = new List<T>();
            using (var conn = dbContext.Database.Connection)
            {
                conn.Open();
                var cmd = new SqlCommand(procName, conn as SqlConnection);
                cmd.CommandType = CommandType.StoredProcedure;
                foreach (var prop in paramList.GetType().GetProperties(BindingFlags.Instance | BindingFlags.Public))
                {
                    cmd.Parameters.AddWithValue(prop.Name, prop.GetValue(paramList, null));
                }
                var reader = cmd.ExecuteReader();
               //result = reader.ReadAllRecords<T>().ToList();
            }
            return result;
        }

        public TEntity Find(int id)
        {
            return Find(p => p.Id == id);
        }

        private DbSet<TEntity> Collection
        {
            get
            {
                return dbContext.Set<TEntity>();
            }
        }
    }
}
