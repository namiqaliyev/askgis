﻿using AskGis.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace AskGis.DAL
{
    public interface IRepository<TEntity> where TEntity : EntityBase
    {
        IQueryable<TEntity> All();
        IQueryable<TEntity> Select(Expression<Func<TEntity, bool>> predicate);
        TEntity Find(Expression<Func<TEntity, bool>> predicate);
        TEntity Find(int id);
        void Insert(TEntity entity);
        void Update(TEntity entity);
        void Delete(TEntity entity);

        object RunQuery<T>(string command, object paramList);
    }
}
