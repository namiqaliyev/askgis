﻿using AskGis.Entities;

namespace AskGis.DAL
{
    public interface IUnitOfWork
    {
        IRepository<TEntity> CreateRepository<TEntity>() where TEntity : EntityBase;
        void SaveChanges();
    }
}