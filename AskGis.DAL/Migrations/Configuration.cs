﻿namespace AskGis.DAL.Migrations
{
    using AskGis.Entities;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<AskGisDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(AskGisDbContext context)
        {
            SeedPagesAndPermissions(context);
            context.SaveChanges();
        }

        private void SeedPagesAndPermissions(AskGisDbContext context)
        {
            context.Pages.RemoveRange(context.Pages);

            var page = new Page
            {
                Title = "Əməkdaşlar",
                Url = "/Persons",
                Controller = "Persons",
                Action = "Index",
                InsertAt = DateTime.Now,
                ModifiedAt = DateTime.Now,
                IsAlive = true
            };
            context.Pages.Add(page);
            page.Permissions.Add(new Permission
            {
                PageId = page.Id,
                Title = "Məlumatlara baxma",
                ShortTitle = "PERSONS_VIEW",
                InsertAt = DateTime.Now,
                ModifiedAt = DateTime.Now,
                IsAlive = true
            });
            page.Permissions.Add(new Permission
            {
                PageId = page.Id,
                Title = "Məlumat daxil etmə",
                ShortTitle = "PERSONS_INSERT",
                InsertAt = DateTime.Now,
                ModifiedAt = DateTime.Now,
                IsAlive = true
            });
            page.Permissions.Add(new Permission
            {
                PageId = page.Id,
                Title = "Məlumatlara düzəliş etmə",
                ShortTitle = "PERSONS_EDIT",
                InsertAt = DateTime.Now,
                ModifiedAt = DateTime.Now,
                IsAlive = true
            });
            page.Permissions.Add(new Permission
            {
                PageId = page.Id,
                Title = "Məlumatları silmə",
                ShortTitle = "PERSONS_DELETE",
                InsertAt = DateTime.Now,
                ModifiedAt = DateTime.Now,
                IsAlive = true
            });

            page = new Page
            {
                Title = "İstifadəçilər",
                Url = "/Users",
                Controller = "Users",
                Action = "Index",
                InsertAt = DateTime.Now,
                ModifiedAt = DateTime.Now,
                IsAlive = true
            };
            context.Pages.Add(page);
            page.Permissions.Add(new Permission
            {
                PageId = page.Id,
                Title = "Məlumatlara baxma",
                ShortTitle = "USERS_VIEW",
                InsertAt = DateTime.Now,
                ModifiedAt = DateTime.Now,
                IsAlive = true
            });
            page.Permissions.Add(new Permission
            {
                PageId = page.Id,
                Title = "Məlumat daxil etmə",
                ShortTitle = "USERS_INSERT",
                InsertAt = DateTime.Now,
                ModifiedAt = DateTime.Now,
                IsAlive = true
            });
            page.Permissions.Add(new Permission
            {
                PageId = page.Id,
                Title = "Məlumatlara düzəliş etmə",
                ShortTitle = "USERS_EDIT",
                InsertAt = DateTime.Now,
                ModifiedAt = DateTime.Now,
                IsAlive = true
            });
            page.Permissions.Add(new Permission
            {
                PageId = page.Id,
                Title = "Məlumatları silmə",
                ShortTitle = "USERS_DELETE",
                InsertAt = DateTime.Now,
                ModifiedAt = DateTime.Now,
                IsAlive = true
            });

            page = new Page
            {
                Title = "Rollar",
                Url = "/Roles",
                Controller = "Roles",
                Action = "Index",
                InsertAt = DateTime.Now,
                ModifiedAt = DateTime.Now,
                IsAlive = true
            };
            context.Pages.Add(page);
            page.Permissions.Add(new Permission
            {
                PageId = page.Id,
                Title = "Məlumatlara baxma",
                ShortTitle = "ROLES_VIEW",
                InsertAt = DateTime.Now,
                ModifiedAt = DateTime.Now,
                IsAlive = true
            });
            page.Permissions.Add(new Permission
            {
                PageId = page.Id,
                Title = "Məlumat daxil etmə",
                ShortTitle = "ROLES_INSERT",
                InsertAt = DateTime.Now,
                ModifiedAt = DateTime.Now,
                IsAlive = true
            });
            page.Permissions.Add(new Permission
            {
                PageId = page.Id,
                Title = "Məlumatlara düzəliş etmə",
                ShortTitle = "ROLES_EDIT",
                InsertAt = DateTime.Now,
                ModifiedAt = DateTime.Now,
                IsAlive = true
            });
            page.Permissions.Add(new Permission
            {
                PageId = page.Id,
                Title = "Məlumatları silmə",
                ShortTitle = "ROLES_DELETE",
                InsertAt = DateTime.Now,
                ModifiedAt = DateTime.Now,
                IsAlive = true
            });
        }
    }
}
