﻿using AskGis.Entities;
using AskGis.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AskGis.DAL
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly AskGisDbContext dbContext;

        public UnitOfWork()
        {
            //dbContext = Helper.CreateDbContext() as AskGisDbContext;
            dbContext = new AskGisDbContext();
        }

        public IRepository<TEntity> CreateRepository<TEntity>() where TEntity: EntityBase
        {
            return new Repository<TEntity>(dbContext);
        }

        public void SaveChanges()
        {
            dbContext.SaveChanges();
        }
    }
}
