﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;

namespace AskGis.Config
{
    public static class Configs
    {
        public static AppSettingsConfig AppSettings { get; set; }

        public static void Initialize()
        {
            //var location = AppDomain.CurrentDomain.DynamicDirectory;
            //var location = GetType().Assembly.Location;
            //var location = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            //var filePath = Path.Combine(location, "appsettings.json");
            //var json = File.ReadAllText(filePath);
            //AppSettings = Newtonsoft.Json.JsonConvert.DeserializeObject<AppSettingsConfig>(json);
            var configReader = new ConfigReader("appsettings.json");
            AppSettings = configReader.Read<AppSettingsConfig>();
        }
    }
}
