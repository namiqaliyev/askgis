﻿using System;

namespace AskGis.Config
{
    [ConfigFile("appsettings.json")]
    public class AppSettingsConfig
    {
        public string DbContextClassPath { get; set; }
        public string PasswordSaltKey { get; set; }
        public int LoginDuration { get; set; }
        public string LogPath { get; set; }
        public bool AllowWithoutLogging { get; set; }
    }
}
