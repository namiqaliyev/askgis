﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AskGis.Config
{
    public class ConfigReader
    {
        private string path;

        public ConfigReader(string fileName)
        {
            path = new Uri(GetType().Assembly.CodeBase).LocalPath;
            path = Path.GetDirectoryName(path);
            path = Path.Combine(path, fileName);
        }

        public T Read<T>()
        {
            var json = File.ReadAllText(path);
            return Newtonsoft.Json.JsonConvert.DeserializeObject<T>(json);
        }
    }
}
