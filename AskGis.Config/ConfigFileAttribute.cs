﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AskGis.Config
{
    public class ConfigFileAttribute: Attribute
    {
        public ConfigFileAttribute(string configFilePath)
        {
            Path = configFilePath;
        }

        public string Path { get; set; }
    }
}
