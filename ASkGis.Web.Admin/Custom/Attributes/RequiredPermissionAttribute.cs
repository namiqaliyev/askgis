﻿using AskGis.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AskGis.Web.Admin.Custom.Attributes
{
    public class RequiredPermissionAttribute: ActionFilterAttribute
    {
        private readonly string permissionName;
        private readonly IAuthBusiness business;

        public RequiredPermissionAttribute(string permissonName)
        {
            permissionName = permissonName;
            business = new AuthBusiness();
        }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var controller = filterContext.HttpContext.Request.RequestContext.RouteData.Values["controller"].ToString();
            var action = filterContext.HttpContext.Request.RequestContext.RouteData.Values["action"].ToString();
            var token = filterContext.HttpContext.Request.Cookies["_tkn"].Value;
            var user = business.GetUserByToken(token);
            var permissions = user.Role.Permissions;
            if (permissions.Count(p => p.ShortTitle == permissionName) == 0)
                throw new Exception($"Sizin {action}@{controller} metoduna səlahiyyətiniz yoxdur.");
        }
    }
}