﻿using AskGis.Business;
using AskGis.Config;
using AskGis.Web.Admin.Custom.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AskGis.Web.Admin.Custom
{
    public abstract class BaseAdminController : BaseController
    {
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (Configs.AppSettings.AllowWithoutLogging)
                return;

            if (!HasToken())
            {
                throw new RequestDoesNotContainTokenException { Request = filterContext.HttpContext.Request };
            }
            else
            {
                var token = GetToken();
                var loginBusiness = new AuthBusiness();
                var isok = loginBusiness.IsTokenFamiliar(token);
                if (!isok)
                {
                    throw new UnfamiliarTokenException { Request = filterContext.HttpContext.Request };
                }
            }

            base.OnActionExecuting(filterContext);
        }
    }
}