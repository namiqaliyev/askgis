﻿using AskGis.Business;
using AskGis.Common.Loging;
using AskGis.Web.Admin.Custom.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AskGis.Web.Admin.Custom
{
    public abstract class BaseController: Controller
    {
        public JsonResult Failure(object data = null)
        {
            Response.StatusCode = 404;
            return Json(new {
                hasError = true,
                data = data
            });
        }

        public JsonResult Success(object data = null)
        {
            Response.StatusCode = 200;
            return Json(new {
                hasError = false,
                data = data
            });
        }

        protected override void OnException(ExceptionContext filterContext)
        {
            var exception = filterContext.Exception;
            filterContext.ExceptionHandled = true;
            Logger.Log(exception.ToString());
            if (exception is RequestDoesNotContainTokenException)
            {
                filterContext.Result = RedirectToAction("Index", "Login");
            }
            else if (exception is UnfamiliarTokenException)
            {
                filterContext.Result = RedirectToAction("Index", "Login");
            }
            else
            {
                if (HttpContext.Request.IsAjaxRequest())
                {
                    filterContext.Result = Failure(new
                    {
                        message = exception.Message
                    });
                }
                else
                {
                    TempData["ErrorData"] = exception;
                    filterContext.Result = RedirectToAction("Index", "Error");
                }
            }
            base.OnException(filterContext);
        }

        protected IEnumerable<string> GetModelStateErrors()
        {
            var list = new List<string>();
            foreach (ModelState modelState in ViewData.ModelState.Values)
            {
                foreach (ModelError error in modelState.Errors)
                {
                    list.Add(error.ErrorMessage);
                }
            }

            return list;
        }

        protected bool HasToken()
        {
            var containsToken = HttpContext.Request.Cookies.AllKeys.Contains("_tkn");
            return containsToken;
        }

        protected string GetToken()
        {
            return HasToken() ? HttpContext.Request.Cookies["_tkn"].Value : string.Empty;
        }
    }
}