﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AskGis.Web.Admin.Custom.Enums
{
    public enum PageType
    {
        List,
        CreateOrEdit,
        Create,
        Edit,
        Delete,
        None
    }
}