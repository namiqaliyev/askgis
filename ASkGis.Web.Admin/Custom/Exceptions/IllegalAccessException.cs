﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AskGis.Web.Admin.Custom.Exceptions
{
    public class IllegalAccessException: Exception
    {
        public HttpRequestBase Request { get; set; }
    }
}