﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AskGis.Web.Admin.Custom.Exceptions
{
    public class RequestDoesNotContainTokenException: Exception
    {
        public HttpRequestBase Request { get; set; }
    }
}