﻿using AskGis.Business;
using AskGis.Entities.InputModels;
using AskGis.Web.Admin.Custom;
using AskGis.Web.Admin.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AskGis.Web.Admin.Controllers
{
    public class RolesController : BaseAdminController
    {
        private readonly IRoleBusiness roleBusiness;
        private readonly IPageBusiness pageBusiness;

        public RolesController()
        {
            roleBusiness = new RoleBusiness();
            pageBusiness = new PageBusiness();
        }

        public ActionResult Index()
        {
            var viewModel = new RoleIndex_ViewModel
            {
                PageTitle = "Rollar",
                Roles = roleBusiness.All()
            };
            return View(viewModel);
        }

        public ActionResult Create(int id = 0)
        {
            var role = roleBusiness.GetById(id);
            if (id != 0 && role == null)
                throw new Exception("Rol tapılmadı");

            var viewModel = new RoleCreate_ViewModel
            {
                PageTitle = id == 0 ? "Yeni Rol" : "Rol məlumatlarının redaktə edilməsi",
                PageType = Custom.Enums.PageType.CreateOrEdit,
                Role = role ?? new Entities.Role(),
                Pages = pageBusiness.All(),              
            };

            return View(viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Save(RoleSaveInput input)
        {
            if (ModelState.IsValid)
            {
                roleBusiness.Save(input);
                return Success();
            }

            var errors = GetModelStateErrors();
            return Failure(errors.ElementAt(0));
        }

        public ActionResult Delete(int id)
        {
            var role = roleBusiness.GetById(id);
            if (role == null)
                throw new Exception("Rol tapılmadı");
            var viewModel = new RoleCreate_ViewModel
            {
                PageTitle = "Rol məlumatlarının silinməsi",
                Role = role,
                PageType = Custom.Enums.PageType.Delete,
                Pages = pageBusiness.All(),
            };
            return View(viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionName("Delete")]
        public ActionResult DeletePost(int id)
        {
            roleBusiness.Delete(id);
            return Success();
        }
    }
}