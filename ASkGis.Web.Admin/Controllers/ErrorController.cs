﻿using AskGis.Web.Admin.Custom;
using AskGis.Web.Admin.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AskGis.Web.Admin.Controllers
{
    public class ErrorController : Controller
    {
        // GET: Error
        public ActionResult Index()
        {
            var exception = TempData["ErrorData"] as Exception;
            var msg = exception != null ? exception.Message : "Xəta baş verdi";

            var viewModel = new ErrorIndex_ViewModel
            {
                PageTitle = "Xəta baş verdi",
                ErrorMessage = msg
            };

            return View(viewModel);
        }
    }
}