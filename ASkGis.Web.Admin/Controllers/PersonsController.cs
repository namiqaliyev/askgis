﻿using AskGis.Business;
using AskGis.Entities.InputModels;
using AskGis.Web.Admin.Custom;
using AskGis.Web.Admin.Custom.Attributes;
using AskGis.Web.Admin.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AskGis.Web.Admin.Controllers
{
    public class PersonsController : BaseAdminController
    {
        private readonly IPersonBusiness personBusiness;

        public PersonsController()
        {
            personBusiness = new PersonBusiness();
        }

        public ActionResult Index()
        {
            var viewModel = new PersonIndex_ViewModel
            {
                PageTitle = "Əməkdaşlar",
                Persons = personBusiness.All()
            };

            return View(viewModel);
        }


        public ActionResult Create(int id = 0)
        {
            var person = personBusiness.GetById(id);
            if (id != 0 && person == null)
                throw new Exception("Redaktə edilmək üçün məlumat tapılmadı");

            var viewModel = new PersonCreate_ViewModel
            {
                PageTitle = id == 0 ? "Yeni əməkdaş" : "Əməkdaşın redaktə edilməsi",
                Person = person ?? new Entities.Person()
            };

            return View(viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Save(PersonSaveInput input)
        {
            if (ModelState.IsValid)
            {
                personBusiness.Save(input);
                return Success(new
                {
                    url = Url.Action("Index")
                });
            }

            var errors = GetModelStateErrors();
            return Failure(new
            {
                message = errors.ElementAt(0)
            });
        }

        public ActionResult Delete(int id)
        {
            var person = personBusiness.GetById(id);
            if (person == null)
                throw new Exception("Silmək üçün məlumat tapılmadı");

            var viewModel = new PersonDelete_ViewModel
            {
                PageTitle = "Əməkdaşın silinməsi",
                Person = person
            };

            return View(viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionName("Delete")]
        [RequiredPermission("PERSONS_DELETE")]
        public ActionResult DeletePost(int personId)
        {
            personBusiness.Delete(personId);
            return Success();
        }
    }
}