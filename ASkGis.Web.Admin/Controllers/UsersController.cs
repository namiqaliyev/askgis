﻿using AskGis.Business;
using AskGis.Entities.InputModels;
using AskGis.Web.Admin.Custom;
using AskGis.Web.Admin.Custom.Enums;
using AskGis.Web.Admin.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AskGis.Web.Admin.Controllers
{
    public class UsersController : BaseAdminController
    {
        private readonly IUserBusiness usersBusiness;
        private readonly IPersonBusiness personsBusiness;
        private readonly IRoleBusiness rolesBusiness;

        public UsersController()
        {
            usersBusiness = new UserBusiness();
            personsBusiness = new PersonBusiness();
            rolesBusiness = new RoleBusiness();
        }

        public ActionResult Index()
        {
            var viewModel = new UsersIndex_ViewModel
            {
                PageTitle = "İstifadəçilər",
                Users = usersBusiness.All()
            };
            return View(viewModel);
        }

        public ActionResult Create(int id = 0)
        {
            var user = usersBusiness.GetById(id);
            if (id != 0 && user == null)
                throw new Exception("İstifadəçi tapılmadı");

            var viewModel = new UsersCreate_ViewModel
            {
                PageTitle = id == 0 ? "Yeni istifadəçi" : "İstifadəçi məlumatlarının redaktə ediilməsi",
                PageType = id == 0 ? PageType.Create : PageType.Edit,
                User = user ?? new Entities.User(),
                PersonsDropDown = personsBusiness.All().Select(p => new SelectListItem
                {
                    Text = $"{p.LastName} {p.FirstName} {p.Fathername}",
                    Value = p.Id.ToString(),
                    Selected = id == 0 ? false : p.Id == user.PersonId
                }),
                RolesDropDown = rolesBusiness.All().Select(p => new SelectListItem
                {
                    Text = p.RoleName,
                    Value = p.Id.ToString(),
                    Selected = id == 0 ? false : p.Id == user.Role.Id
                })
            };

            return View(viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Save(NewUserInput input)
        {
            if (ModelState.IsValid)
            {
                usersBusiness.Save(input);
                return Success(new
                {
                    url = Url.Action("Index")
                });
            }

            var errors = GetModelStateErrors();
            return Failure(new
            {
                message = errors.ElementAt(0)
            });
        }

        public ActionResult Delete(int id)
        {
            var user = usersBusiness.GetById(id);
            if (user == null)
                throw new Exception("İstifadəçi tapılmadı");

            var viewModel = new UsersDelete_ViewModel
            {
                PageTitle = "İstifadə məlumatının silinməsi",
                User = user
            };

            return View(viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionName("Delete")]
        public ActionResult DeletePost(int id)
        {
            usersBusiness.Delete(id);
            return Success(new
            {
                url = Url.Action("Index")
            });
        }

        public ActionResult ChangePassword(int id)
        {
            var user = usersBusiness.GetById(id);
            if (user == null)
                throw new Exception("İstifadəçi tapılmadı");
            var viewModel = new UsersChangePassword_ViewModel
            {
                PageType = PageType.Edit,
                PageTitle = "İstifadəçi şifrəsinin dəyişdirilməsi",
                User = user
            };

            return View(viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ChangePassword(ChangePasswordInput input)
        {
            if (ModelState.IsValid)
            {
                usersBusiness.ChangePassword(input);
                return Success();
            }

            var errors = GetModelStateErrors();
            return Failure(new
            {
                message = errors.ElementAt(0)
            });
        }
    }
}