﻿using AskGis.Business;
using AskGis.Entities.InputModels;
using AskGis.Utils;
using AskGis.Web.Admin.Custom;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AskGis.Web.Admin.Controllers
{
    public class LoginController : BaseController
    {
        private readonly IAuthBusiness loginBusiness;

        public LoginController()
        {
            loginBusiness = new AuthBusiness();
        }

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginInput input)
        {
            if (ModelState.IsValid)
            {
                var user = loginBusiness.GetUserByUsername(input.Username);
                if(!EncDec.BCrypt_IsEqual(input.Password, user.Password))
                {
                    throw new Exception("Şifrə və ya istifadəçi düzgün daxil edilməyib.");
                }

                var token = Token.Create();
                loginBusiness.Login(user, token);
                Response.SetCookie(new HttpCookie("_tkn", token));
                return RedirectToAction("Index", "Home");
            }

            return Json(new { });
        }

        [HttpPost]
        public ActionResult Logout()
        {
            if (HasToken())
            {
                var token = GetToken();
                loginBusiness.Logout(token);
                return Success();
            }
            return Failure();
        }
    }
}