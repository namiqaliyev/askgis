﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AskGis.Entities;
using AskGis.Web.Admin.Custom.Enums;

namespace AskGis.Web.Admin.Models.ViewModels
{
    public class PersonDelete_ViewModel: BaseViewModel
    {
        public override PageType PageType { get => PageType.Delete; set => base.PageType = value; }
        public Person Person { get; set; }
        public string Gender
        {
            get => Person.IsGenderMale ? "Kişi" : "Qadın";
        }
    }
}