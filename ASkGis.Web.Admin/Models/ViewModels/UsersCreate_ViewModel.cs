﻿using AskGis.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AskGis.Web.Admin.Models.ViewModels
{
    public class UsersCreate_ViewModel: BaseViewModel
    {
        public User User { get; set; }
        public IEnumerable<SelectListItem> PersonsDropDown { get; set; }
        public IEnumerable<SelectListItem> RolesDropDown { get; set; }
    }
}