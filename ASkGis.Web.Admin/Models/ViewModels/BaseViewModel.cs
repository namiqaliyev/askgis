﻿using AskGis.Web.Admin.Custom.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AskGis.Web.Admin.Models.ViewModels
{
    public class BaseViewModel
    {
        public string PageTitle { get; set; }
        public virtual PageType PageType { get; set; }
        public object Tag { get; set; }
    }
}