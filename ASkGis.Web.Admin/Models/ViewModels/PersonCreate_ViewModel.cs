﻿using AskGis.Entities;
using AskGis.Web.Admin.Custom.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AskGis.Web.Admin.Models.ViewModels
{
    public class PersonCreate_ViewModel: BaseViewModel
    {
        public override PageType PageType { get => PageType.CreateOrEdit; set => base.PageType = value; }
        public Person Person { get; set; }
    }
}