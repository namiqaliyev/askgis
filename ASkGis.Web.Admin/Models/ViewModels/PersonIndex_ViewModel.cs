﻿using AskGis.Entities;
using AskGis.Web.Admin.Custom.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AskGis.Web.Admin.Models.ViewModels
{
    public class PersonIndex_ViewModel: BaseViewModel
    {
        public IEnumerable<Person> Persons { get; set; }

        public override PageType PageType { get => PageType.List; set => base.PageType = value; }
    }
}