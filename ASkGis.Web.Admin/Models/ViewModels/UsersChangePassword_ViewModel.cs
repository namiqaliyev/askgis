﻿using AskGis.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AskGis.Web.Admin.Models.ViewModels
{
    public class UsersChangePassword_ViewModel: BaseViewModel
    {
        public User User { get; set; }
        public string Fullname
        {
            get
            {
                return $"{User.Person.LastName} {User.Person.FirstName} {User.Person.Fathername}";
            }
        }
    }
}