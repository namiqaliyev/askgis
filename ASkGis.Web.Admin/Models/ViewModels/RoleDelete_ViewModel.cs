﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AskGis.Entities;
using AskGis.Web.Admin.Custom.Enums;

namespace AskGis.Web.Admin.Models.ViewModels
{
    public class RoleDelete_ViewModel: BaseViewModel
    {
        public override PageType PageType { get => PageType.Delete; set => base.PageType = value; }

        public Role Role { get; set; }

        public IEnumerable<Page> Pages { get; set; }
    }
}