﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AskGis.Web.Admin.Models.ViewModels
{
    public class ErrorIndex_ViewModel: BaseViewModel
    {
        public string ErrorMessage { get; set; }
    }
}