﻿using AskGis.Entities;
using AskGis.Web.Admin.Custom.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AskGis.Web.Admin.Models.ViewModels
{
    public class UsersDelete_ViewModel: BaseViewModel
    {
        public User User { get; set; }

        public string Fullname => $"{User.Person.LastName} {User.Person.FirstName} {User.Person.Fathername}";

        public override PageType PageType { get => PageType.Delete; set => base.PageType = value; }
    }
}