﻿$(function () {


    $(document).on("click", "#logout-button", function () {
        $.post("/login/logout", function () {
            document.location.reload();
        });
    });
    
});


function submit_form(e) {
    e.preventDefault();
    var $target = $(e.target);
    var target_form = $target.data("target-form");
    var form = document.getElementById(target_form);
    form = $(form);
    var data = form.serialize();
    var url = form.attr("action");
    $target.html("<i class='fa fa-refresh fa-spin'></i>&nbsp;Yaddaş...").attr("disabled", "disabled");
    $.post(url, data, function (response) {
        bootbox.alert("Əməliyyat uğurla sona çatdı");
    }).fail(function (response) {
        bootbox.alert(response.responseJSON.data.message);
    }).always(function () {
        $target.html("<i class='fa fa-save'></i>&nbsp;Yaddaş").removeAttr("disabled");
    });
}

function remove(e) {
    e.preventDefault();
    var $target = $(e.target);
    var target_form = $target.data("target-form");
    var form = document.getElementById(target_form);
    form = $(form);
    var data = form.serialize();
    var url = form.attr("action");
    $target.html("<i class='fa fa-refresh fa-spin'></i>&nbsp;Sil...").attr("disabled", "disabled");
    $.post(url, data, function (response) {
        bootbox.alert("Əməliyyat uğurla sona çatdı");
    }).fail(function (response) {
        bootbox.alert(response.responseJSON.data.message);
    }).always(function () {
        $target.html("<i class='fa fa-save'></i>&nbsp;Sil").removeAttr("disabled");
    });
}

function search(e, clear) {
    e.preventDefault();
    var clear = clear || false;
    var elem = e.target;
    var target_form_id = $(elem).data("target-form");
    var form = document.forms[target_form_id];
    if (clear)
        form.reset();

    target_form_id = "#" + target_form_id;
    var target_div = "#" + $(target_form_id).data("target-div");
    var search_summary_id = "#" + $(target_form_id).data("search-summary");
    var url = $(target_form_id).attr("action");
    var data = $(form).serialize();

    $.post(url, data, function (response) {
        $(target_div).html(response.data.htmlContent);
        if (clear) {
            $(search_summary_id).empty();
        } else {
            $(search_summary_id).html("[Məlumat sayı: " + response.data.rowCount + "]");
        }
    });
}

