﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AskGis.Utils
{
    public static class Token
    {
        public static string Create()
        {
            var guid = Guid.NewGuid();
            return guid.ToString("N");
        }
    }
}
