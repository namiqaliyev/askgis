﻿using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace System
{
    public static class ExtensionMethods
    {
        //-----------------------------------------------------------------------------
        // String extensions
        //-----------------------------------------------------------------------------
        public static bool IsEmpty(this string str)
        {
            return string.IsNullOrEmpty(str);
        }

        public static bool IsNotEmpty(this string str)
        {
            return !str.IsEmpty();
        }

        //-----------------------------------------------------------------------------
        // IDataReader.ReadAllRecords
        //-----------------------------------------------------------------------------
        public static IEnumerable<T> ReadAllRecords<T>(this IDataReader reader)
        {
            var array = new List<T>();
            var propertyNames = typeof(T).GetProperties().Select(p => p.Name).ToList();
            var columnNames = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();
            while (reader.Read())
            {
                var item = Activator.CreateInstance<T>();
                foreach (var propName in propertyNames)
                {
                    var colName = GetProperColName(propName, columnNames.ToArray());
                    if (string.IsNullOrEmpty(colName))
                        continue;

                    var propType = item.GetType().GetProperty(propName).PropertyType;
                    var readValue = reader[colName];
                    var value = readValue == DBNull.Value ? GetDefaultValue(propType) : Convert.ChangeType(readValue, propType);
                    item.GetType().GetProperty(propName).SetValue(item, value);
                }
                array.Add(item);
            }
            reader.Close();
            return array;
        }

        private static string GetProperColName(string propName, string[] colNames)
        {
            foreach (var col in colNames)
            {
                var s1 = Normalize(col).ToUpperInvariant();
                var s2 = propName.ToUpperInvariant();
                if (s1 == s2)
                    return col;
            }
            return string.Empty;
        }

        private static string Normalize(string s)
        {
            var result = s.Replace("_", "");
            return result;
        }

        private static object GetDefaultValue(Type type)
        {
            if (type.IsValueType)
                return Activator.CreateInstance(type);
            return null; ;
        }
    }
}
