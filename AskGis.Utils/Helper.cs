﻿using AskGis.Config;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AskGis.Utils
{
    public static class Helper
    {
        public static object CreateDbContext()
        {
            var dbContextClassPath = Configs.AppSettings.DbContextClassPath;
            //var arr = dbContextClassPath.Split('@');
            var type = Type.GetType(dbContextClassPath);
            return Activator.CreateInstance(type);
        }
    }
}
