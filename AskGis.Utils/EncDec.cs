﻿using AskGis.Config;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace AskGis.Utils
{
    public static class EncDec
    {
        public static string ToBase64(string text)
        {
            var bytes = Encoding.UTF8.GetBytes(text);
            var textEncoded = Convert.ToBase64String(bytes);
            return textEncoded;
        }

        public static string FromBase64(string text)
        {
            var bytes = Convert.FromBase64String(text);
            var textDecoded = Encoding.UTF8.GetString(bytes);
            return textDecoded;
        }

        public static string MD5_Hash(string input)
        {
            var hash = new StringBuilder();
            var hashCode = Configs.AppSettings.PasswordSaltKey;
            var md5provider = new MD5CryptoServiceProvider();
            byte[] bytes = md5provider.ComputeHash(new UTF8Encoding().GetBytes(input + hashCode));

            for (int i = 0; i < bytes.Length; i++)
            {
                hash.Append(bytes[i].ToString("x2"));
            }
            return hash.ToString();
        }

        public static string BCrypt_Hash(string input)
        {
            var salt = DevOne.Security.Cryptography.BCrypt.BCryptHelper.GenerateSalt();
            return DevOne.Security.Cryptography.BCrypt.BCryptHelper.HashPassword(input, salt);
        }

        public static bool BCrypt_IsEqual(string plaint_text, string hashed_password)
        {
            return DevOne.Security.Cryptography.BCrypt.BCryptHelper.CheckPassword(plaint_text, hashed_password);
        }
    }
}
