﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace AskGis.Entities
{
    [Table("Roles")]
    public class Role : EntityBase
    {
        public Role()
        {
            Permissions = new List<Permission>();
        }

        [Required]
        [StringLength(30)]
        public string RoleName { get; set; }

        public virtual List<Permission> Permissions { get; set; }
    }
}
