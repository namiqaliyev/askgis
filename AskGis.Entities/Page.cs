﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace AskGis.Entities
{
    [Table("PageList")]
    public class Page : EntityBase
    {
        public Page()
        {
            Permissions = new List<Permission>();
        }

        [Required]
        [StringLength(30)]
        public string Title { get; set; }

        [Required]
        [StringLength(100)]
        public string Url { get; set; }

        [Required]
        [StringLength(20)]
        public string Controller { get; set; }

        [Required]
        [StringLength(20)]
        public string Action { get; set; }

        public virtual IList<Permission> Permissions { get; set; }
    }
}
