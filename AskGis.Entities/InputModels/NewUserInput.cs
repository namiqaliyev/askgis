﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AskGis.Entities.InputModels
{
    public class NewUserInput
    {
        public int UserId { get; set; }

        [Required(ErrorMessage = "Əməkdaş seçilməyib")]
        public int PersonId { get; set; }

        [Required(ErrorMessage = "Rol seçilməyib")]
        public int RoleId { get; set; }

        [Required(ErrorMessage = "İstifadəçi adı daxil edilməyib")]
        [RegularExpression(@"[a-zA-Z0-9]+", ErrorMessage = "Yalnız ingilis əlifbasının kiçik, böyük hərfləri və 0-dan 9-a kimi rəqəmlər istifadə edilə bilər")]
        [StringLength(100, MinimumLength = 3)]
        public string Username { get; set; }

        [Required(ErrorMessage = "Şifrə daxil edilməyib")]
        [StringLength(100, MinimumLength = 3)]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Compare("Password", ErrorMessage = "Şifrələr eyni deyil")]
        public string PasswordAgain { get; set; }
    }
}
