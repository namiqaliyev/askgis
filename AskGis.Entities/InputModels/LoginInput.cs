﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AskGis.Entities.InputModels
{
    public class LoginInput
    {
        [Required(ErrorMessage = "İstifadəçi adı daxil edilməyib")]
        [RegularExpression(@"[a-zA-Z0-9]+", ErrorMessage = "Yalnız ingilis əlifbasının kiçik, böyük hərfləri və 0-dan 9-a kimi rəqəmlər istifadə edilə bilər")]
        [StringLength(100, MinimumLength = 3)]
        public string Username { get; set; }

        [Required(ErrorMessage = "Şifrə daxil edilməyib")]
        [StringLength(100, MinimumLength = 3)]
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
}
