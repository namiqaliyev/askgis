﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AskGis.Entities.InputModels
{
    public class PersonSaveInput
    {
        public int PersonId { get; set; }

        [Required(ErrorMessage = "Əməkdaşın adı daxil edilməyib")]
        [RegularExpression(@"\w+", ErrorMessage = "Yalnız hərflərdən istifadə edilə bilər")]
        [StringLength(20, MinimumLength = 3)]
        public string Firstname { get; set; }

        [Required(ErrorMessage = "Əməkdaşın ata adı daxil edilməyib")]
        [RegularExpression(@"\w+", ErrorMessage = "Yalnız hərflərdən istifadə edilə bilər")]
        [StringLength(20, MinimumLength = 3)]
        public string Fathername { get; set; }

        [Required(ErrorMessage = "Əməkdaşın soyadı daxil edilməyib")]
        [RegularExpression(@"\w+", ErrorMessage = "Yalnız hərflərdən istifadə edilə bilər")]
        [StringLength(20, MinimumLength = 3)]
        public string Lastname { get; set; }

        [Required(ErrorMessage = "Cinsiyyət seçilməyib")]
        [Range(1, 2, ErrorMessage = "Cinsiyyət seçilməyib")]
        public int Gender { get; set; }
    }
}
