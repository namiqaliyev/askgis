﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AskGis.Entities.InputModels
{
    public class ChangePasswordInput
    {
        [Required(ErrorMessage = "İstifadəçi seçilməyib")]
        public int UserId { get; set; }

        [Required(ErrorMessage = "Şifrə daxil edilməyib")]
        [StringLength(100, MinimumLength = 3)]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Compare("Password", ErrorMessage = "Şifrələr eyni deyil")]
        public string PasswordAgain { get; set; }
    }
}
