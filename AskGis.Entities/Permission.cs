﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace AskGis.Entities
{
    [Table("PermissionList")]
    public class Permission: EntityBase
    {
        public int PageId { get; set; }

        [Required]
        [StringLength(100)]
        public string Title { get; set; }

        [Required]
        [StringLength(100)]
        public string ShortTitle { get; set; }

        public virtual ICollection<Role> Roles { get; set; }
    }
}
