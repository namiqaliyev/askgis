﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AskGis.Entities
{
    [Table("LoginActivity")]
    public class LoginActivity: EntityBase
    {
        public int UserId { get; set; }
        public virtual User User { get; set; }

        [Required]
        [StringLength(250)]
        public string Token { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime LoginTime { get; set; }

        public bool IsActive { get; set; }
    }
}
