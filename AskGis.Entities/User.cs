﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace AskGis.Entities
{
    [Table("Users")]
    public class User: EntityBase
    {
        [StringLength(20)]
        [Required]
        public string Username { get; set; }

        [StringLength(255)]
        [Required]
        public string Password { get; set; }

        public bool IsAdmin { get; set; }

        public int RoleId { get; set; }

        public virtual Role Role { get; set; }

        public int PersonId { get; set; }

        public virtual Person Person { get; set; }
    }
}
