﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace AskGis.Entities
{
    [Table("Persons")]
    public class Person: EntityBase
    {
        [Required]
        [StringLength(30)]
        public string FirstName { get; set; }

        [StringLength(30)]
        public string Fathername { get; set; }

        [Required]
        [StringLength(30)]
        public string LastName { get; set; }

        public bool IsStaff { get; set; }

        public bool IsGenderMale { get; set; }
    }
}
