﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AskGis.Entities
{
    public abstract class EntityBase
    {
        [Key]
        public int Id { get; set; }

        [Column(TypeName = "datetime2")]
        //[DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime InsertAt { get; set; }

        [Column(TypeName = "datetime2")]
        //[DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime ModifiedAt { get; set; }

        //[DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public bool IsAlive { get; set; }
    }
}
